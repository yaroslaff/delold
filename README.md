# delold

Delete old backup files

## How it works
Process files in directory specified by `-d/--dir`, (and in each of its subdirectories, if `-r/--recursive` specified).

For each file it *cleans* it  (assume *deletes* for now, but see below for details) unless one of checks is successful.

Checks are performed in this order:


### Initial check

#### Minsize
Files with size less then `--minsize <size>` are never deleted (they are ignored). Size can have suffix K, M or G (lowercase is okay). 
Use `--minsize 0` to delete even small files.

### Policies

Policies are given in with `--policy` option. More then one policy can be given, for example:
~~~
./delold.py -d /tmp/test/ --policy 7:week:0 30:firstmonth --minsize 1M
~~~

Here 7 and 30 are 'selectors', and 'week' is policy (with argument 0) and 'firstmonth' is policy too(without arguments).

Rules here are:
- Ignore (never delete) files with size less then 1M
- Ignore (do not delete) files with age less then 7 days (because no policy matches it).
- For files with age 7-31 days - keep files which are created (mtime) on monday and delete other files.
- For files older then 31 days we keep only one file for each month for each 'template'.

#### week
Has one argument, day of week (by number).
Matches when file is created in this day. e.g. week:0 matches for files created on Monday, and week:6 for files created on Sunday.

Note: age of file based on file mtime.

#### firstmonth
Matches only for first file or this template in this month in directory. For example, if we have:
~~~
serverA-20180101.tar.gz
serverA-20180102.tar.gz
...
serverA-20180131.tar.gz
serverB-20180201.tar.gz
serverB-20180202.tar.gz
~~~
(and same set of backup files for serverB)

After running --firstmonth policy, we will have 4 files: 
~~~
serverA-20180101.tar.gz
serverA-20180201.tar.gz
serverB-20180101.tar.gz
serverB-20180201.tar.gz
~~~
All other files will be deleted.

See also 'Templates' below.

Note: age of file based on file mtime (Not by date in filename).

#### df
Delete files based on free disk space. Argument is free disk space we try to have. If disk is used for more then N percents, files will be cleaned.
~~~
./delold.py -d /tmp/test --policy 0:df:20 --maxdel 1 -v --rm
~~~
This will clean files from /tmp/test if disk usage is over 20%. 

df:0 will match almost always (unless you really have less then 1% usage on disk).

This options is very useful on special backup machines, when first run of delold with `--recycle` (`--mv`) option moves backups to special directory (your 'recycle bin'), and 2nd run of delold checks this directory and deletes files if you have low free space. This way you will have old backups in recycle bin, not deleted, if you have enough free space.

### Templates
Files with similar names are grouped by one timestamp template name. Template specified by --regex/--re option, default is [0-9][0-9_\-]{5,} (6 or more chars in timestamp, started with number, possible with - or _ characters).

For example, if you have three these backup files, here are corresponding template names. 
~~~
serverA-20180101.tar.gz > serverA-DATE.tar.gz
serverA-20180201.tar.gz > serverA-DATE.tar.gz
serverB-20180101.tar.gz > serverB-DATE.tar.gz
~~~
 
When firstmonth policy will process this directory, when it will process  serverA backups, it will match (keep) only one (oldest by mtime) file for serverA and only one for serverB.

### Recursive
If `-r`/`--recursive` is specified, it will dive into subdirectories. But each subdirectory is processed independently. (Same effect as if you run delold many times, once for each directory, with same options)

This is important to remember for --maxdel options and firstmonth policy (if you have serverA backups in two directories, when delold will process files in first directory, it will not know about files in other directory).

### Maxdel
Delete no more then `--maxdel <N>` files. Option is specific to directory. So, if you have structure like:
~~~~
backups/
    serverA/
        serverA-20180101.tar.gz
        serverA-20180201.tar.gz
    serverB/
        serverB-20180101.tar.gz
        serverB-20180201.tar.gz
~~~~ 
running ./delold.py on backups/ with --maxdel 1 can delete two files. one in serverA and one in serverB

### Dry run
If `--dry` is specified, delold will have 'dry run' - will not make any changes (no deletion, no moving)

### --delete/--rm
If specified, old backups will be deleted

*If no --rm and no --mv options given, or if --dry given - it will work 'read only'*

### --recycle/--mv 
If specified, old backups will be moved to directory (argument to --mv option).

*If no --rm and no --mv options given, or if --dry given - it will work 'read only'*

