#!/usr/bin/python

import os, sys, configargparse, re, datetime, shutil, logging

#
# less then week: keep all files
# less then month: keep monday
# other: keep first in month
#


def kmg(s):
    try:
        return int(s)
    except ValueError:
        ints = int(s[:-1])
        suffix = s[-1].upper()
        
        if suffix=='K':
            return ints*1024
        if suffix=='M':
            return ints*1024*1024
        if suffix=='G':
            return ints*1024*1024*1024
        assert(False)
        

def cleanfile(path, delete, recycle, dry):
    if dry:
        log.info('dry run. do not clean {}'.format(path))
        return
    if recycle:
        basename = os.path.basename(path)
        newpath = os.path.join(recycle, basename)
        log.info("RECYCLE {} to {}".format(path, newpath))
        shutil.move(path, newpath)
    elif delete:
        log.info("DELETE {}".format(path))
        os.remove(path)
        

def keepfile(f, policy):
    log.debug('SAVEFILE {} kept by policy {}'.format(f['basename'], policy))

def getfm(files, tpl, dt, verbose=False):
    """
        return fullpath for first backup file in month (with this tpl name, in month from dt)
    """
        
    tf = filter(lambda x: x['tpl'] == tpl and x['datetime'].year == dt.year and x['datetime'].month == dt.month, files)
    
    minf = None
    for f in tf:
        if minf is None or f['datetime'] < minf['datetime']:
            minf = f
    return minf
    
def cleanup(files, policy, minsize, delete, recycle, maxdel, dry, qfile):
    tpls = list() 
    for f in files:
        if not f['tpl'] in tpls:
            tpls.append(f['tpl'])

    cleaned=0

    policies = sorted(policy.keys())

    files = sorted(files, key=lambda k: k['mtime'], reverse=True) # oldest first 
                
    for f in files:

        verbose = f['full'] == qfile

        if verbose:
            print f

        # log.debug('cleanup? {} age: {}'.format(f['basename'], f['age']))
        if f['size'] < minsize:
            log.debug('keep {}, size {} less then minsize {}'.format(f['basename'], f['size'], minsize))
            if verbose:
                log.info('keep {}, size {} less then minsize {}'.format(f['basename'], f['size'], minsize)) 
            continue

    
        if f['age'] < policies[0]:
            log.debug('keep {}, age {} less then min policy age {}'.format(f['basename'], f['age'], policies[0]))
            if verbose:
                log.info('keep {}, age {} less then min policy age {}'.format(f['basename'], f['age'], policies[0])) 
            continue
        
        # get policy for it

                
        
        p = None
        for pi in reversed(policies):            
            if verbose:
                print "consider", pi
            if f['age'] >= pi:      
                p = policy[pi]
                if verbose:
                    print "pick this policy:", p
                break

        if verbose:
            print "Policy for this file:", p


        # here we must have policy         
        if p is None:
            log.error("Not found policy")
            log.error("Policies: "+str(policies))
            log.error("pi: " + str(pi))
            log.error("age: " + str(f['age']))
            os.exit(1)
            
                               
        #log.debug('apply policy {} to {}'.format(p, f['basename']))
        if p.startswith('week:'):
            wk = int(p.split(':')[1])
            if wk == f['datetime'].weekday():
                keepfile(f, policy)
            else:
                if maxdel is None or maxdel>cleaned:
                    cleanfile(f['full'], delete, recycle, dry)
                    cleaned += 1
                else:
                    keepfile(f,'maxdel')
       
        elif p == 'firstmonth':
            fm = getfm(files, f['tpl'], f['datetime'], verbose)
            if f['full'] == fm['full']:
                keepfile(f, policy)
            else:
                if maxdel is None or maxdel>cleaned:
                    cleanfile(f['full'], delete, recycle, dry)
                    cleaned += 1
                else:
                    keepfile(f,'maxdel')
        elif p.startswith('df:'):
            dflimit = int(p.split(':')[1])

            stat = os.statvfs(f['full'])
            df = 100 - (stat.f_bfree*100 / stat.f_blocks)

            if df>dflimit:
                # too low disk space
                if maxdel is None or maxdel>cleaned:
                    cleanfile(f['full'], delete, recycle, dry)
                    cleaned += 1
                else:
                    keepfile(f,'maxdel')
                 
        else:
            log.error('Unknown policy \'{}\''.format(p))
            assert(False)
            
           
    
    
        
def process(path, policy, recursive, minsize, delete, recycle, regex, maxdel, dry, qfile):
    print "processing", path

    files = list()
    
    for basename in os.listdir(path):
        full = os.path.join(path, basename)
        if os.path.isdir(full):
            log.debug("DIR "+full)
            if recursive:
                process(full, 
                    recursive=recursive, 
                    policy=policy,
                    minsize=minsize, 
                    delete=delete,
                    recycle=recycle,
                    regex=regex,
                    maxdel=maxdel,
                    dry=dry)
            else:
                log.debug('skip dir '+full)

        elif os.path.isfile(full):
            tpl = re.sub(regex, 'DATE', basename)
            f = dict()
            f['basename'] = basename
            f['full'] = full
            f['tpl'] = tpl
            f['mtime'] = os.path.getmtime(full)
            f['datetime'] = datetime.datetime.fromtimestamp(f['mtime'])
            f['age'] = (datetime.datetime.now() - f['datetime']).days 
            f['size'] = os.stat(full).st_size
            files.append(f)

    cleanup(files, policy, minsize, delete=delete, recycle=recycle, maxdel=maxdel, dry=dry, qfile=qfile)





p = configargparse.ArgParser(default_config_files=['~/delold.conf', '/etc/delold.conf'], description='delete old files')
#p = argparse.ArgumentParser(description='delete old files')

#print p
p.add('-c', '--config', is_config_file=True, help='config file path')
p.add('-v', '--verbose', help='verbose', action='store_true')
p.add('-q', '--quiet', help='quiet', action='store_true')
p.add('--minsize', help='minimal file size. suffixes k/m/g supported', default='1M')
p.add('--policy', nargs='+', default=['7:week:0','31:firstmonth'])
p.add('-d','--dir', default=None, required=True)
p.add('-r','--recursive', default=False, help='recursive', action='store_true')
p.add('--delete','--rm', default=False, help='delete old files', action='store_true')
p.add('--recycle','--mv', default=None, help='move to this folder')
p.add('--regex','--re', default='[0-9][0-9_\-]{5,}', help='date regex')
p.add('--dry', default=False, action='store_true', help='dry run')
p.add('--qfile', metavar='FILENAME',default=None, help='question about filename.')
p.add('--maxdel', default=None, type=int, help='max allowed deletion in one directory')
args = p.parse_args()

if args.qfile:
    args.dry = True

logging.basicConfig(format = '%(message)s', level = logging.INFO)
log = logging.getLogger('lxc-backup')

if args.verbose:
    # err = logging.StreamHandler(sys.stderr)
    # log.addHandler(err)
    log.setLevel(logging.DEBUG)
elif args.quiet:
    log.setLevel(logging.WARNING)


minsize = kmg(args.minsize)

policy = dict()
for p in args.policy:
    age, f = p.split(':',1)
    policy[int(age)] = f


process(args.dir, policy, recursive = args.recursive, minsize=minsize, delete=args.delete, recycle=args.recycle, regex=args.regex, maxdel=args.maxdel, dry=args.dry, qfile=args.qfile)
