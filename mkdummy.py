#!/usr/bin/python

import argparse
import datetime
import time
import os

p = argparse.ArgumentParser(description='generate dummy files')
p.add_argument('-n', type=int, required=True, help='number of files to generate')
p.add_argument('-t', '--template', default='/tmp/dummy-DATE.tar.gz', help='template. DATE will be replaced to YYYYMMDD. /tmp/dummy-{}.tar.gz')
p.add_argument('-s', '--size', default=0, type=int, help='size of file. default is 0')

args = p.parse_args()
print args

d = datetime.datetime.now()

fmt = args.template.replace('DATE','{}')

for n in xrange(args.n):  
    date = d.strftime('%Y%m%d')      
    unixtime = time.mktime(d.timetuple())
    filename = fmt.format(date)
    print "make file", n, filename, unixtime
    
    
    with open(filename, 'w') as f:
        f.write('x' * args.size)
    os.utime(filename, (unixtime, unixtime))
        
    d -= datetime.timedelta(days=1) 

